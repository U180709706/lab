package stack;

import java.util.ArrayList;

public class StackArrayImpl implements Stack{
	ArrayList stack = new ArrayList();
	
	public void push(Object item) {
		stack.add(item);
	}

	public Object pop() {
		return stack.remove(stack.size()-1));
	}

	public boolean empty() {
		return stack.isEmpty();
	}

}
