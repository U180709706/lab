package stack;

public class StackImpl implements Stack {
	StackItem top;

	public void push(Object item) {
		if (top == null) {
			top = new StackItem(item);
			return;
		}

		StackItem next = top;
		while (true) {
			if (next.getNext() != null)
				next = next.getNext();
			else
				break;
		}
		next.setNext(new StackItem(item));
	}

	public Object pop() {
		if (top == null)
			return null;
		StackItem owner = top;
		StackItem next = top;
		while (true) {
			if (next != null && next.getNext() != null) {
				owner = next;
				next = next.getNext();
			} else
				break;
		}
		if (next == top)
			top = null;
		else
			owner.setNext(null);
		return next.getItem();
	}

	public boolean empty() {
		return top == null;
	}

}
