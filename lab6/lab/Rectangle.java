package lab;


public class Rectangle {
	int sideA ;
    int sideB ;
    
    Point topLeft ;
    
    Rectangle(int x, int y, Point A){
        sideA = x;
        sideB = y;
        topLeft = A ;
    }
    
    int area(){
        int theArea = sideA * sideB;
        return (theArea);
    }
    
    int perimeter(){
        int thePerimeter = 2*(sideA + sideB);
        return (thePerimeter);
    }
    
     public Point[] corner() {
    	Point topRight = new Point((topLeft.xCoord + sideA), topLeft.yCoord);
    	Point bottomLeft = new Point(topLeft.xCoord, (topLeft.yCoord - sideB));
    	Point bottomRight = new Point((topLeft.xCoord + sideA),
    									(topLeft.yCoord - sideB));
    	
    	Point ArrayList[] = new Point[4];
    	ArrayList[0] = topLeft;
    	ArrayList[1] = topRight;
    	ArrayList[2] = bottomLeft;
    	ArrayList[3] = bottomRight;
    	
    	return (ArrayList);
    }
	

}
