package lab;



public class Main {
	
	public static void main(String[] args) {
		Point A = new Point(2,2);
        Rectangle rectangle = new Rectangle(2 , 2 , A);
        
        Circle circle1 = new Circle(10, new Point(2,2));
        Circle circle2 = new Circle(10, new Point(2,2));
        
        System.out.println("The area is " + rectangle.area() + "\n" +
        					"The perimeter is " + rectangle.perimeter());
        
        Point ar[] = rectangle.corner();
        System.out.println("The corners are ");
        for (int i = 0 ; i < 4 ; i ++) {
        	
        	System.out.println( "    " +  ar[i].xCoord + " " + ar[i].yCoord);
        }
        
        
        
        System.out.println( "The area of the circle " + circle1.area() + "\n" 
        					+"The perimeter of the circle " + circle1.perimeter());
        
       if (circle2.intersect(circle1))
    	   System.out.println("They are intersect .");
       else System.out.println("They are not intersect .");
    }
	
	

}
