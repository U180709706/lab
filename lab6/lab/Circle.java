package lab;



public class Circle {
	
	int radius;
    Point center;
    
    Circle(int x, Point A){
        radius = x;
        center = A;
    }
    
    double area(){
        return ((3.14)*((radius)*(radius)));
    }
    
    double perimeter(){
        return (2*(3.14)*(radius));
    }
    
    boolean intersect(Circle A){
        double d = ((center.xCoord - A.center.xCoord)*(center.xCoord - A.center.xCoord) + 
                     (center.yCoord - A.center.yCoord )*(center.yCoord -A.center.yCoord))*(0.5);
        int sum = radius + A.radius;
        
        if (d <= sum ) return true;
        
        return false;
    }
}
