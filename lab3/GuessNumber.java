
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		//Random rand = new Random(); //Creates an object from Random class
		//int number =rand.nextInt(100); //generates a number between 0 and 99
		int number = 50;
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
        int guess = reader.nextInt(); //Read the user input
        int count = 1;
        
        if (guess != number){
            do{
                System.out.println("Sorry ");
                if (guess > number) 
                    System.out.println("Mine in less than you guess");
                else 
                    System.out.println("Min is greater than your guess ");
                System.out.println("Type -1 to quit or Guess another number:");
                guess = reader.nextInt();     
                count ++;   
            }while (guess != -1 &&  guess != number);
            if (guess == -1){
                System.out.println("Sorry, the number is :" + number);
            }
            else  System.out.println("congra , you won after " + count + " attempts");

        }
        else System.out.println("congran , you won after " + count + " attempts");    
		
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
