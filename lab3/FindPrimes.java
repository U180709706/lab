public class FindPrimes {

    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);

        for(int x = 2 ; x < number; x++ ){
            if (isPrime(x) == true){
                System.out.print(x + ",");
            }
        }
            System.out.print("\n");
    }

    public static boolean isPrime(int x){
        for(int j =2; j < x; j++){
            if (x % j == 0){
                return false;
                
            }
        }
        return true;

    }
    
}
