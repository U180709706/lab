
import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
                int count =0;
                int row;
                int col;
		printBoard(board);

                while (count < 9){
                    
                    do {
                        System.out.print("Player 1 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 1 enter column number:");
                        col = reader.nextInt();
                    } while ((board[row - 1][col - 1] =='X') ||(board[row - 1][col - 1] =='O') 
                            ||(row > 4 || col > 4 || col < 0 || row < 0));
                    System.out.println("Vaild Move");
                    
                    board[row - 1][col - 1] = 'X';
                    printBoard(board);
                    if (isWin(board, row-1,col-1)){
                        System.out.println("Player 1 has won");
                        break;
                        }
                    count ++;
                    
                    do{
                        System.out.print("Player 2 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 2 enter column number:");
                        col = reader.nextInt();
                    } while ((board[row - 1][col - 1] == 'X') || (board[row - 1][col - 1] =='O') 
                            ||(row > 4 || col > 4 || col < 0 || row < 0));
                    System.out.println("Vaild move ");
                    board[row - 1][col - 1] = 'O';
                    printBoard(board);
                    if (isWin(board, row-1,col-1)){
                         System.out.println("Player 1 has won");
                        break;
                    }
                    
                    count ++;
                    
                }
		reader.close();
	}
        
        
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
         public static boolean isWin(char[][] board, int rowLast, int colLast) {
		char symbol = board[rowLast][colLast];
		
		boolean win=true;
		for (int col= 0; col <3;col++) {
			if (board[rowLast][col]!=symbol) {
					win =false;
					break;
			}
		}
			if (win) {
				return true;
			}
		
		
		win=true;
		for (int row= 0; row <3;row++) {
			if (board[row][colLast]!=symbol) {
				win =false;
				break;
			}
		}
		if (win) {
			return true;
		}
		
		
		win=true;
		
		if (rowLast==colLast) {
			for (int loc=0;loc<3;loc++) {
				if(board[loc][loc]!=symbol) {
					win=false;
					break;
				}
			}
			if(win) {
				return true;				
			}
		}
		
	
		if (rowLast+colLast==2) {
			win=true;
			for (int row=0;row<3;row++) {
				if(board[row][2-row]!=symbol) {
					win=false;
					break;
				}
			}
			if(win) {
				return true;				
			}
		}
			
		
            return false;
	}

}

