
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence{
        
        public static int[] rowIndex = new int[10];
                public static int[] colIndex = new int[10];
	public static void main(String[] args) throws FileNotFoundException{
		int matrix[][] = readMatrix();
                
		boolean found = false;
search: for (int i=0; i< matrix.length; i++) {
			for (int j=0; j < matrix[i].length; j++) {
				if (search(0,matrix, i,j)){
					found = true;
					break search;
				}
			}
		}
			
		if (found) {
			System.out.println("A sequence is found");
                     printMatrix(matrix);
		}
            }                
       
        
	private static boolean search (int number, int[][]matrix, int row, int col) {
             
            
            if (matrix[row][col] == number){
                rowIndex[number] = row;
                colIndex[number] = col;
               
                if ( number == 9) {
                    for (int i = 0 ; i < 10 ; i++){
                        matrix[rowIndex[9-i]][colIndex[9-i]] = i;
                }
                    return true;
                }
                else{
                    if (row != 0 && row != (matrix.length -1) && col != 0 && col != (matrix.length -1)){
                        if ( matrix[row +1][col] == number+1){
                            return (search(number+1 , matrix, row+1 , col));
                        }
                        else if ( matrix[row -1][col] == number+1){
                            return (search(number+1 , matrix, row-1 , col));
                        }
                        else if ( matrix[row][col +1] == number+1){
                            return (search(number+1 , matrix, row , col +1));
                        }
                        else if ( matrix[row][col-1] == number+1){
                            return (search(number+1 , matrix, row , col-1));
                        }
                    }    
                    
                }
                
            }
             
                return false; 
            }
            
                

	private static int[][] readMatrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("lab5/matrix.txt"); // if you run from command line use  new File("matrix.txt") instead

		try (Scanner sc = new Scanner(file)){

			
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}

