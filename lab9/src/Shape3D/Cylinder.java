/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape3D;

/**
 *
 * @author taha
 */
import Shape2D.Circle;

public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
            super();
            height = 1.0;
    }

    public Cylinder(double radius, double height) {
            super(radius);
            this.height = height;
    }

    public double getHeight() {
            return height;
    }

    public void setHeight(double height) {
            this.height = height;
    }

    public double volume() {
            return super.area() * height;
    }

    public double area() {
            return (super.area() * 2) + (height * 2 * Math.PI * getRadius());
    }

    public String toString() {
            return "Cylinder[radius=" + getRadius() + ", height=" + height + "]";
    }

}

