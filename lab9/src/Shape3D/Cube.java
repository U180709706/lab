/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape3D;

/**
 *
 * @author taha
 */

import Shape2D.Square;

public class Cube extends Square {
	public Cube() {
		super();
	}

	public Cube(double side) {
		super(side);
	}

	public double area() {
		return 6 * super.area();
	}

	public double volume() {
		return super.area() * getSide();
	}

	public String toString() {
		return "Cube[side=" + getSide() + "]";
	}
}
    

