/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape3D;

/**
 *
 * @author taha
 */
public class TestShape3D {
    public static void main(String args[]) {
            Cube cube = new Cube(3.0);
            Cylinder cylinder = new Cylinder(3.0, 3.0);
            System.out.println("Area of " + cube + "=" + cube.area());
            System.out.println("Volume of " + cube + "=" + cube.volume());
            System.out.println("Area of " + cylinder + "=" + cylinder.area());
            System.out.println("Volume of " + cylinder + "=" + cylinder.volume());
    }
    
}
