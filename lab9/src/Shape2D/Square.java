/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape2D;

/**
 *
 * @author taha
 */
public class Square {
    
    private double side;

    public Square() {
            side = 1.0;
    }

    public Square(double side) {
            this.side = side;
    }

    public double getSide() {
            return side;
    }

    public void setSide(double side) {
            this.side = side;
    }

    public double area() {
            return side * side;
    }

    public String toString() {
            return "Square[side=" + side+"]";
    }
    
}
