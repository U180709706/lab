/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape2D;

/**
 *
 * @author taha
 */

import Shape3D.Cube;
public class TestShapes2D {
    
    public static void main(String args[]) {
            Square square = new Square(3.0);
            Circle circle = new Circle(3.0);
            System.out.println("Area of " + square + "=" + square.area());
            System.out.println("Area of " + circle + "=" + circle.area());
    }
    
}
