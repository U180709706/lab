/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape2D;

/**
 *
 * @author taha
 */
public class Circle {
    private double radius;

    public Circle() {
            radius = 1.0;
    }

    public Circle(double radius) {
            this.radius = radius;
    }

    public double getRadius() {
            return radius;
    }

    public void setRadius(double d) {
            this.radius = d;
    }

    public double area() {
            return Math.PI * radius * radius;
    }

    public String toString() {
            return "Circle[radius=" + radius+"]";
    }
    
}
